## Build Instruction

<hr />

1. Download a more advanced package manager called `yarn` by `npm`. It is a better choice than `npm` because `yarn` keeps a repository of packages in local such that it reduces greatly the time necessary to download common packages at least compared to the older version of `npm`.

   ```bash
   npm install --global yarn
   ```

2. Go to below 2 folders to install package with yarn

   ```bash
   cd healthtest_api
   yarn intsall
   ```

   ```bash
   cd ../healthtest_react
   yarn intsall
   ```

3. Create a database at PostgreSQL with your user info

   ```bash
   CREATE DATABASE health_test
   ```

4. Build a `.env.development` file with `.env.development.sample` format in backend. Also build another `.env` file with `.env.sample` info in frontend

   ```bash
   cd healthtest_api
   touch .env.development
   ```

   ```bash
   cd ../healthtest_react
   touch .env
   ```

5. Run KnexJS commands as query builder at `healthtest_api` to create tables and dummy user into the database

   ```bash
   cd ../healthtest_api
   yarn knex migrate:up
   yarn knex seed:run
   ```

6. Start the server at `healthtest_api`to prepare to request and response data to front-end side and connect with database

   ```bash
   ts-node main.ts
   ```

   Start React at `healthtest_react`to run the web on browser

   ```
   cd ../healthtest_react
   yarn start
   ```

7. Open browser and go to `http://localhost:3000/`. Enter the email and password as marked at `healthtest_api/seeds/create-dummy-user.ts` then you check user's personal data and genetic result
   ```bash
   email: 'tony.stark@stark.com',
   password: 'qweasdzxc'
   ```

<hr />

### Database Design

![ERD](/health_test_app.png)
In this database, a table of `users` is sufficient for this service. Most columns' type in `users` is `VARCHAR` as those data is in `string`. The type of column of `policy_code` and `genetic_result` is a bit special. Specific type is applied for `policy_code` because it requires a 8-digit alphanumeric string. Therefore `CREATE DOMAIN` function in `psql` is being used to create a specific type called `alphanum`, to ensure every record of `policy_code` is with 8-digit alphanumeric string (`VARCHAR(8)[A-Z0-9]{8}`). For `genetic_result`, it applies `jsonb` type as it is quicker to read the JSON record instead of using `json` type.

KnexJS is applied to fulfill data migration as query builder of creating or dropping tables can be run easily. About concurrency, increasing the maximum pool at `healthtest_api/knexfile.ts`and using `.transaction(trx)`instead of `knex`at every SQL command can enhance the database capacity.

About security, not disclosing any database info in codes but creating an environment file, not pushing the `.env.development` to git and ensuring marking it in `.gitignore` can prevent security problem.

<hr/>

### API Design

`ExpressJS` is applied as RESTful API to represent data and transfer it over HTTP by calling an URL. One of the benefits of a RESTful architecture is its ability to be cached, to solve the server processing problem. AJAX is applied to make asynchronous HTTP requests as well, to reduce full page loads to update the state of UI and data modification. With RESTful and AJAX should be able to reduce server load and network traffic

On the security level, there are no any messages like user password can be reached from front-end as the calculation is handled at server-side. `BCryptJS` is being used to hash every new password. With 'salt round' set as 10 means that the calculation is done 2^10 times, it is safe enough to keep the hashed password. Moreover, `jwt` is being used to do login guard by checking token with secret preset at server-side.

<hr/>

### Testing Strategy

`Jest` will be considered to apply on test cases. **Integration Test** and then **End-to-End Testing** can be applied for this service. Integration test is a test for multiple units that can call database and read/write in file system before the user interface is implemented. In this service the main function is to get user data from database at server-side, using integration test is able to examine the logic of those functions and correct output. In final stage, E2E testing can be applied that it goes from the very frontend (browser) until the very backend (database), to ensure the UI is functioning together with the backend and reflecting the data received from database.
