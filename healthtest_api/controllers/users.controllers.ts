import express from 'express';
import jwtSimple from 'jwt-simple'

import { UserService } from "../services/users.services";
import { checkPassword, hashPassword } from "../utils/hash";
import { IUser } from '../utils/models';
import jwt from '../utils/jwt';

export class UserController {
    constructor(public userService: UserService) { }

    login = async (req: express.Request, res: express.Response) => {
        let { email, password } = req.body;

        try {
            let user = await this.userService.loadUserByEmail(email);
            const date = user.date_of_birth.getDate()
            const month = user.date_of_birth.getMonth() + 1
            const year = user.date_of_birth.getFullYear()
            const birthday = year + '-' + month + '-' + date

            if (user) {
                let checkPW = await checkPassword(password, user.password);
                if (!checkPW) {
                    res.status(401).json({ msg: "Wrong Username/Password" });
                }
                const payload: IUser = {
                    id: user.id,
                    email: user.email,
                    first_name: user.first_name,
                    last_name: user.last_name,
                    date_of_birth: birthday,
                    policy_code: user.policy_code,
                    genetic_result: user.genetic_result
                };
                const token = jwtSimple.encode(payload, jwt.jwtSecret);
                res.json({ token: token, user: JSON.stringify(payload) });

            } else {
                res.status(401).json({ msg: "Wrong Username/Password" });
            }
        } catch (error) {
            res.status(500).json(error.toString());
        };
    };

    getUserInfo = async (req: express.Request, res: express.Response) => {
        try {
            const user = req.user;
            await res.json(user);
        } catch (error) {
            console.log("Getting user info controller error: ", error.toString());
            res.status(500).json({ msg: "Error on restoring user info." });
        };
    };

    register = async (req: express.Request, res: express.Response) => {
        let { email, password, first_name, last_name, date_of_birth } = req.body;
        let emailError = ''

        try {
            console.log('New user info in controller: ', req.body)

            let isEmailDuplicated = await this.userService.loadUserByEmail(email);
            if (isEmailDuplicated) {
                emailError = 'Email has been registered already.'
            };
            if (!emailError) {
                const hashedPW = await hashPassword(password);
                await this.userService.saveNewUser({
                    email: email,
                    password: hashedPW,
                    first_name: first_name,
                    last_name: last_name,
                    date_of_birth: date_of_birth,
                });
                return;
            };

            let newUser = await this.userService.loadUserByEmail(email);
            const payload: IUser = {
                id: newUser.id,
                email: newUser.email,
                first_name: newUser.first_name,
                last_name: newUser.last_name,
                date_of_birth: newUser.date_of_birth,
                policy_code: newUser.policy_code,
                genetic_result: newUser.genetic_result
            };
            const token = jwtSimple.encode(payload, jwt.jwtSecret);

            res.json({ token: token, user: JSON.stringify(payload) });
        } catch {
            res.status(500).json({ msg: "Internal Server Error for Registration" });
            return;
        };
    };
};
