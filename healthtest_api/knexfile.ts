import dotenv from 'dotenv';
import fs from 'fs';


let mode = process.env.NODE_ENV || 'development';
console.log('Environment Mode: ', mode);
let envFile = '.env.' + mode;
let envFileContent = fs.readFileSync(envFile).toString()
let env = dotenv.parse(envFileContent);

let configs = {

    test: {
        client: 'postgresql',
        connection: {
            database: env.TEST_DB_NAME,
            user: env.DB_USERNAME,
            password: env.DB_PASSWORD,
            host: env.DB_HOST,
        },
        pool: {
            min: 2,
            max: 50
        },
        migrations: {
            tableName: "knex_migrations"
        }
    },

    development: {
        client: 'postgresql',
        connection: {
            database: env.DB_NAME,
            user: env.DB_USERNAME,
            password: env.DB_PASSWORD,
            host: env.DB_HOST,
        },
        pool: {
            min: 2,
            max: 50
        },
        migrations: {
            tableName: "knex_migrations"
        }
    }

};

module.exports = configs;
export default configs;
