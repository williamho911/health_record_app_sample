import express from 'express';
import expressSession from 'express-session';
import fs from 'fs';
import { knex } from 'knex';
import dotenv from 'dotenv';
import cors from 'cors';
import bodyParser from 'body-parser';

import knexConfigs from './knexfile';

// Call knexfile to apply the configs and create connection with MySQL
let mode = process.env.NODE_ENV || 'development';
let knexConfig = knexConfigs[mode];
export const knexInit = knex(knexConfig);
export const env = dotenv.parse(fs.readFileSync('.env.' + mode).toString())

const app = express();
app.use(expressSession({
    secret: "Your health care app",
    resave: true,
    saveUninitialized: true
}));
app.use(cors());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(express.static("public"));

import { UserService } from './services/users.services';
import { UserController } from './controllers/users.controllers';
import { loginGuard } from './utils/guard';

const userService = new UserService(knexInit)
export const isLoggedIn = loginGuard(userService)
export const userController = new UserController(userService)

// Access to different routes
import { routes } from "./routes";
const API_VERSION = "/";
app.use(API_VERSION, routes);

// Set up localhost server
const PORT = 8080;

app.listen(PORT, () => {
    console.log(`Server runs at ${PORT} in ${mode} mode.`)
});

app.use((req, res) => {
    res.redirect('/404.html')
    console.log('URL: ', req.url, '; METHOD: ', req.method)
});

