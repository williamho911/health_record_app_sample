import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
    await knex.raw(`
    CREATE DOMAIN alphanum AS VARCHAR(8) CHECK (VALUE ~ '^[A-Z0-9]{8}$')
    `);

    if (await knex.schema.hasTable('users')) {
        return;
    };

    await knex.schema.createTable('users', table => {
        table.increments();
        table.string('email', 60).unique().notNullable();
        table.string('password', 60).notNullable();
        table.string('first_name', 60).notNullable();
        table.string('last_name', 60).notNullable();
        table.date('date_of_birth');
        table.specificType('policy_code', 'alphanum');
        table.jsonb('genetic_result');
        table.timestamps(false, true);
    })

}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTableIfExists('users');
    await knex.raw(`
    DROP DOMAIN alphanum CASCADE
    `);

}

