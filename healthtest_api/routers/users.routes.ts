import { userController, isLoggedIn } from '../main'
import express from 'express';


export const userRoute = express.Router()

userRoute.post('/login', userController.login);
userRoute.get('/userInfo', isLoggedIn, userController.getUserInfo);
userRoute.post('/register', userController.register)
