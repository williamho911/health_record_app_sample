import express from "express";
import { userRoute } from "./routers/users.routes";

export const routes = express.Router()
routes.use('/', userRoute);