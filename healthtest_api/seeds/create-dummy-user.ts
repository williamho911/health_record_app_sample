import { Knex } from "knex";

import { IUser } from '../utils/models';
import { hashPassword } from '../utils/hash'

export async function seed(knex: Knex): Promise<void> {
    // Deletes ALL existing entries
    await knex("users").del();

    // const reg = new RegExp('^[0-9A-Z]{8}$')

    const dummyUser: IUser = {
        email: 'tony.stark@stark.com',
        password: await hashPassword('qweasdzxc'),
        first_name: 'Tony',
        last_name: 'Stark',
        date_of_birth: new Date(1970, 4, 29),
        policy_code: 'A978BC6D',
        genetic_result: JSON.stringify({
            Age: 53,
            Gender: 'M',
            Race: 'Human',
            Nationality: 'American',
            Occupation: ['Avenger', 'Stark Industries CEO', 'Weapons Manufacturer'],
            PhysicalInfo: [{
                Eyes: 'Brown',
                Skin: 'Fair',
                Hair: 'Black',
                Physique: 'Muscular'
            }]
        })
    }

    // Inserts seed entries
    await knex("users").insert(dummyUser);
};
