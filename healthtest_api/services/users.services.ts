import { Knex } from "knex";
import { IUser } from '../utils/models';

export class UserService {
    constructor(public knex: Knex) { }

    async loadUserByEmail(email: string) {
        const result = await this.knex.transaction(async (trx) => {
            return await trx
                .select('*')
                .from('users')
                .where('email', email)
                .first();
        })
        return result;
    };

    async saveNewUser(user: IUser) {
        const newUser = await this.knex.transaction(async (trx) => {
            return await trx.insert({
                email: user.email,
                password: user.password,
                first_name: user.first_name,
                last_name: user.last_name,
                date_of_birth: user.date_of_birth,
            })
                .into('users')

        })
        return newUser
    };
};
