import { Bearer } from 'permit';
import jwtSimple from 'jwt-simple';
import { Request, Response, NextFunction } from 'express';

import jwt from './jwt';
import { UserService } from '../services/users.services';

const permit = new Bearer({ query: 'access_token' });

export function loginGuard(userService: UserService) {
    return async function isLoggedIn(req: Request, res: Response, next: NextFunction) {
        try {
            const token = permit.check(req);
            if (!token || token === null) {
                res.status(401).json({ msg: 'Permission Denied ' });
            };
            const payload = jwtSimple.decode(token, jwt.jwtSecret);
            const user = await userService.loadUserByEmail(payload.email);

            if (user) {
                delete user.password;
                const date = user.date_of_birth.getDate()
                const month = user.date_of_birth.getMonth() + 1
                const year = user.date_of_birth.getFullYear()
                const birthday = year + '-' + month + '-' + date
                user.date_of_birth = birthday
                req.user = user;
                return next();
            } else {
                return res.status(401).json({ msg: "Permission Denied" });
            }
        } catch (error) {
            res.status(500).json({ err: error });
        }
    }
}

