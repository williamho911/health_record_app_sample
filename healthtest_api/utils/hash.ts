import * as bcrypt from 'bcryptjs';

const SALT_ROUUDS = 10;

// Creat functions to hash password and check hashed password
export async function hashPassword(plainPassword: string) {
    const hash = await bcrypt.hash(plainPassword, SALT_ROUUDS);
    return hash;
};

export async function checkPassword(plainPassword: string, hashPassword: string) {
    const match = await bcrypt.compare(plainPassword, hashPassword);
    return match;
};