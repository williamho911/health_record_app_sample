export interface IUser {
    id?: number,
    email?: string,
    password?: string,
    first_name?: string,
    last_name?: string,
    date_of_birth?: Date | string | null,
    policy_code?: string | null,
    genetic_result?: string | null,
}

declare global {
    namespace Express {
        interface Request {
            user?: IUser;
        }
    }
}