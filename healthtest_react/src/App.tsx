import React, { useEffect } from 'react';
import { history, IRootState } from './redux/store';
import { useDispatch, useSelector } from 'react-redux';
import { ConnectedRouter } from "connected-react-router";

import Routes from './components/Routes';
import { restoreLoginThunk } from './redux/auth/thunk';
import Loading from './components/Loading';

function App() {
  const dispatch = useDispatch();
  const isAuthenticated = useSelector(
    (state: IRootState) => state.auth.isAuthenticated
  );
  console.log(`AUTH: ${isAuthenticated}`)
  useEffect(() => {
    if (isAuthenticated === null) {
      // To verify token
      dispatch(restoreLoginThunk());
    }
  }, [isAuthenticated, dispatch])

  return (
    <ConnectedRouter history={history}>
      {
        isAuthenticated === null &&
        <Loading />
      }
      {
        isAuthenticated !== null &&
        <>
          <Routes />
        </>
      }
    </ConnectedRouter>
  );
}

export default App;
