import React from 'react'
import { Link } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import HomeIcon from '@material-ui/icons/Home';
import MeetingRoomIcon from '@material-ui/icons/MeetingRoom';

import { useStyles } from '../css/Common';
import { logoutThunk } from '../redux/auth/thunk';


const ButtonGroup: React.FC = () => {
    const classes = useStyles()
    const dispatch = useDispatch();

    const logout = () => {
        dispatch(logoutThunk())
    }

    return (
        <div className={classes.iconGroup}>
            <div className={classes.iconContainer}>
                <Link to='/'>
                    <HomeIcon className={classes.iconBtn} />
                </Link>
            </div>
            <div className={classes.iconContainer}>
                <Link to='/' onClick={logout}>
                    <MeetingRoomIcon className={classes.iconBtn} />
                </Link>
            </div>
        </div>
    )
}

export default ButtonGroup
