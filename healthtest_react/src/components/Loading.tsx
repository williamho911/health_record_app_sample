import React from 'react';
import { css } from "@emotion/react";
import DotLoader from 'react-spinners/DotLoader';

const styles = {
    spinnerStyle: {
        flex: 1,
        marginTop: '32vh',
        justifyContent: 'center',
        alignItems: 'center'
    }
};


const Loading: React.FC = () => {
    return (
        <div style={styles.spinnerStyle}>
            <DotLoader
                size={84}
                color={'#FFF'}
                css={css`
                    display: block;
                    margin: 0 auto;
                    height: 12vh;
                    width: 12vw;
                    text-align: center;
                    flex: 1;
                    align-self: center;
                `}
            />
        </div>
    )
}

export default Loading
