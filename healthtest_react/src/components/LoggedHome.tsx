import React from 'react'
import { Paper } from '@material-ui/core';
import { Link } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';

import { useStyles, BasicBtn } from '../css/Common';
import { IRootState } from '../redux/store';
import { logoutThunk } from '../redux/auth/thunk';

const LoggedHome: React.FC = () => {
    const classes = useStyles()
    const dispatch = useDispatch();
    const userInfo = useSelector((state: IRootState) => state.auth.user)

    const logout = () => {
        dispatch(logoutThunk())
    }

    return (
        <div>
            <h1 className={classes.titleFontColor}>Welcome Back, {userInfo?.first_name}</h1>
            <Paper className={classes.paper}>
                <div className={classes.btnGroup}>
                    <div className={classes.textInsidePaper}>Your are able to check your:</div>
                    <Link to={{ pathname: "/profile", state: userInfo }} style={{ textDecoration: 'none' }}>
                        <BasicBtn variant="outlined">
                            Profile
                        </BasicBtn>
                    </Link>
                    <div className={classes.textInsidePaper}>or</div>
                    <Link to={{ pathname: "/geneticResult", state: userInfo?.genetic_result }} style={{ textDecoration: 'none' }}>
                        <BasicBtn>
                            Genetic Result
                        </BasicBtn>
                    </Link>
                    <div style={{ height: 20 }} />
                    <div className={classes.textInsidePaper}>Or if you would like to leave, please click:</div>

                    <Link to='/' onClick={logout} style={{ textDecoration: 'none' }}>
                        <BasicBtn >
                            Logout
                        </BasicBtn>
                    </Link>
                </div>
            </Paper>
        </div>
    )
}

export default LoggedHome
