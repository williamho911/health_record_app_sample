import React from 'react'
import { FormControl, Paper, TextField } from "@material-ui/core";

import { useDispatch, useSelector } from "react-redux";
import { useForm } from "react-hook-form";
import { IRootState } from '../redux/store';
import { loginThunk } from '../redux/auth/thunk';
import Loading from './Loading';
import { useStyles, BasicBtn } from '../css/Common';

interface formInput {
    email: string,
    password: string
}

export const Login: React.FC = () => {
    const dispatch = useDispatch();
    const classes = useStyles();
    const isLoginProcessing = useSelector(
        (state: IRootState) => state.auth.isLoginProcessing
    )
    const { register, handleSubmit } = useForm<formInput>()

    const onSubmit = (data: formInput) => {
        if (!isLoginProcessing) {
            dispatch(loginThunk(data.email, data.password))
        }
    }

    if (isLoginProcessing) {
        return <Loading />
    }

    return (
        <>
            <h1
                className={classes.titleFontColor}
                style={{ textAlign: 'center' }}
            >
                Welcome to Your Body Carer
            </h1>
            <Paper className={classes.paper}>
                <h3 className={classes.textInsidePaper}>Login</h3>
                <form onSubmit={handleSubmit(onSubmit)}>
                    <FormControl fullWidth={true}>
                        <TextField
                            className={classes.textFieldColor}
                            label='Email address'
                            type="text"
                            id="email"
                            // name="email"
                            required
                            {...register("email")}
                        ></TextField>
                    </FormControl>
                    <FormControl fullWidth={true}>
                        {/* <InputLabel style={{ color: '#0a0a0a' }} htmlFor="password">Password</InputLabel> */}
                        <TextField
                            className={classes.textFieldColor}
                            label='Password'
                            type="password"
                            id="password"
                            // name="password"
                            required
                            {...register("password")}
                        ></TextField>
                    </FormControl>
                    <div className={classes.btnGroup}>
                        <div className="submit-btn">
                            <BasicBtn type="submit" variant="outlined">
                                Submit
                            </BasicBtn>
                        </div>
                    </div>
                </form>
            </Paper>
        </>
    )
}

