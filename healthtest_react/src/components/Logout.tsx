import React from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { NavLink } from 'react-router-dom';

import { IRootState } from '../redux/store';
import { logoutThunk } from '../redux/auth/thunk';

export const Logout: React.FC = () => {
    const dispatch = useDispatch();
    const isAuthenticated = useSelector(
        (state: IRootState) => state.auth.isAuthenticated
    );

    const logout = () => {
        dispatch(logoutThunk())
    }

    return (
        <div>
            {isAuthenticated && (
                <NavLink to='/' onClick={logout} >
                    LOGOUT
                </NavLink>
            )}
        </div>
    )
}

