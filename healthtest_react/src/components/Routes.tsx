import React from 'react'
import { Route, Switch } from 'react-router-dom'

import { PrivateRoute } from './PrivateRoute';
import Home from '../pages/Home';
import Profile from '../pages/Profile';
import Genetic from '../pages/Genetic';

const Routes: React.FC = () => {
    return (
        <Switch>
            <Route path='/' exact={true} component={Home} />
            <PrivateRoute path="/profile" component={Profile} />
            <PrivateRoute path="/geneticResult" component={Genetic} />
        </Switch>
    )
}

export default Routes
