import { withStyles } from '@material-ui/core/styles';
import { Grid } from '@material-ui/core';
import {
    makeStyles,
    Button,
} from "@material-ui/core";

export const useStyles = makeStyles((theme) => ({
    container: {
        display: "flex",
        flexDirection: "column",
        justifyContent: "center",
        alignItems: "center",
        height: "100vh",
    },
    paper: {
        padding: "12px 12px",
        width: "480px",
        textAlign: "center",
        [theme.breakpoints.down('xs')]: {
            width: '88vw',
        },
    },
    btnGroup: {
        display: 'flex',
        flexDirection: 'column',
        marginTop: 20,
        justifyContent: "center",
        alignItems: "center",
    },
    textFieldColor: {
        "& .MuiInput-underline": {
            '&:after': {
                borderBottom: "2px solid #a365b6 "
            }
        },
        '& .MuiFormLabel-root.Mui-focused': {
            color: "#a365b6"
        }
    },
    containerGrid: {
        padding: 4,

    },
    iconBtn: {
        color: '#947fab',
        width: 40,
        height: 40
    },
    iconContainer: {
        width: 60,
        height: 60,
        margin: 4,
        background: "#FFF",
        borderRadius: 30,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center'
    },
    iconGroup: {
        display: 'flex',
        marginTop: 12
    },
    titleFontColor: {
        color: "#E3D7ED"
    },
    textInsidePaper: {
        color: '#6e5f80',
        padding: '4px 0 4px'
    }
}));

export const BasicBtn = withStyles({
    root: {
        background: 'linear-gradient(90deg, #a365b6 0%, #c289d4 100%);',
        borderRadius: 4,
        border: 0,
        boxShadow: '0 3px 5px 2px rgba(163, 101, 182, .3)',
        color: '#FFF',
        fontWeight: 800,
        margin: '4px 0 4px',
    },
})(Button)

export const InsideGrid = withStyles({
    root: {
        margin: "4px 8px 4px 8px",
        background: "linear-gradient(90deg, #e8dcef 50%, #fff 100%)",
        borderRadius: 4,
    }
})(Grid)