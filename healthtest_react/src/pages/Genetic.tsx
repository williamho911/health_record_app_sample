import React from 'react'
import { Grid } from '@material-ui/core'
import { Card } from '@material-ui/core'
import { useStyles, InsideGrid } from '../css/Common';
import ButtonGroup from '../components/ButtonGroup';

const Genetic = (props: any) => {
    const classes = useStyles()
    const geneticResult = props.location.state
    const { Age, Gender, Nationality, Occupation, PhysicalInfo, Race } = geneticResult
    const { Eyes, Hair, Physique, Skin } = PhysicalInfo[0]

    return (
        <>
            <div className={classes.container}>
                <Card className={classes.paper} style={{ textAlign: 'left' }}>
                    <h2
                        className={classes.textInsidePaper}
                        style={{ margin: 4 }}>
                        Genetic Result
                    </h2>
                    <Grid container className={classes.containerGrid}>
                        <Grid item xs={4} lg={4}>
                            <div>Age:</div>
                        </Grid>
                        <Grid item xs={8} lg={8}>
                            <div>{Age}</div>
                        </Grid>
                    </Grid>
                    <Grid container className={classes.containerGrid}>
                        <Grid item xs={4} lg={4}>
                            <div>Gender:</div>
                        </Grid>
                        <Grid item xs={8} lg={8}>
                            <div>{Gender}</div>
                        </Grid>
                    </Grid>
                    <Grid container className={classes.containerGrid}>
                        <Grid item xs={4} lg={4}>
                            <div>Nationality:</div>
                        </Grid>
                        <Grid item xs={8} lg={8}>
                            <div>{Nationality}</div>
                        </Grid>
                    </Grid>
                    <Grid container className={classes.containerGrid}>
                        <Grid item xs={4} lg={4}>
                            <div>Occupation:</div>
                        </Grid>
                        <Grid item xs={8} lg={8}>
                            {Occupation.map((job: any) => {
                                return (
                                    <div style={{ paddingBottom: 2 }}>{job}</div>
                                )
                            })}
                        </Grid>
                    </Grid>
                    <Grid container className={classes.containerGrid}>
                        <Grid item xs={4} lg={4}>
                            <div>Physical:</div>
                        </Grid>
                    </Grid>
                    <InsideGrid container className={classes.containerGrid}>
                        <Grid item xs={4} lg={4}>
                            <div>Eyes: </div>
                        </Grid>
                        <Grid item xs={8} lg={8}>
                            <div style={{ paddingBottom: 2 }}>{Eyes}</div>
                        </Grid>
                        <Grid item xs={4} lg={4}>
                            <div>Hair: </div>
                        </Grid>
                        <Grid item xs={8} lg={8}>
                            <div style={{ paddingBottom: 2 }}>{Hair}</div>
                        </Grid>
                        <Grid item xs={4} lg={4}>
                            <div>Physique: </div>
                        </Grid>
                        <Grid item xs={8} lg={8}>
                            <div style={{ paddingBottom: 2 }}>{Physique}</div>
                        </Grid>
                        <Grid item xs={4} lg={4}>
                            <div>Skin: </div>
                        </Grid>
                        <Grid item xs={8} lg={8}>
                            <div style={{ paddingBottom: 2 }}>{Skin}</div>
                        </Grid>
                    </InsideGrid>
                    <Grid container className={classes.containerGrid}>
                        <Grid item xs={4} lg={4}>
                            <div>Race:</div>
                        </Grid>
                        <Grid item xs={8} lg={8}>
                            <div>{Race}</div>
                        </Grid>
                    </Grid>
                </Card>
                <ButtonGroup />
            </div>
        </>
    )
}

export default Genetic
