import React from 'react'
import { useSelector } from 'react-redux';

import { Login } from '../components/Login'
import { IRootState } from '../redux/store';
import LoggedHome from '../components/LoggedHome';
import { useStyles } from '../css/Common';

const Home: React.FC = () => {
    const isAuthenticated = useSelector((state: IRootState) => state.auth.isAuthenticated)
    const classes = useStyles()

    return (

        <>
            <div className={classes.container}>
                {!isAuthenticated && (
                    <Login />
                )}
                {isAuthenticated && (
                    <LoggedHome />
                )}
            </div>
        </>
    )
}

export default Home
