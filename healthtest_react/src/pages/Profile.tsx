import React from 'react'
import { Grid } from '@material-ui/core'
import { Card } from '@material-ui/core'
import { useStyles } from '../css/Common';
import ButtonGroup from '../components/ButtonGroup';




const Profile: React.FC = (props: any) => {
    const classes = useStyles()
    const userInfo = props.location.state
    const { email, first_name, last_name, date_of_birth } = userInfo

    return (
        <>
            <div className={classes.container}>
                <Card className={classes.paper} style={{ textAlign: 'left' }}>
                    <h2
                        className={classes.textInsidePaper}
                        style={{ margin: 4 }}>
                        Personal Info
                    </h2>
                    <Grid container className={classes.containerGrid}>
                        <Grid item xs={4} lg={4}>
                            <div>Name:</div>
                        </Grid>
                        <Grid item xs={8} lg={8}>
                            <div>{first_name} {last_name}</div>
                        </Grid>
                    </Grid>
                    <Grid container className={classes.containerGrid}>
                        <Grid item xs={4} lg={4}>
                            <div>Email:</div>
                        </Grid>
                        <Grid item xs={8} lg={8}>
                            <div>{email}</div>
                        </Grid>
                    </Grid>
                    <Grid container className={classes.containerGrid}>
                        <Grid item xs={4} lg={4}>
                            <div>Date of Birth:</div>
                        </Grid>
                        <Grid item xs={8} lg={8}>
                            <div>{date_of_birth}</div>
                        </Grid>
                    </Grid>
                </Card>
                <ButtonGroup />
            </div>
        </>
    )
}

export default Profile
