import { IUserInfo } from "./state";

export const LOGIN_PROCESS = '@@AUTH/LOGIN_PROCESS'
export const LOGIN_SUCCESS = '@@AUTH/LOGIN_SUCCESS'
export const LOGIN_FAILURE = '@@AUTH/LOGIN_FAILURE'
export const LOGOUT = '@@/LOGIN_LOGOUT'
export const REGISTER_PROCESS = '@@AUTH/REGISTER_PROCESS'
export const REGISTER_SUCCESS = '@@AUTH/REGISTER_SUCCESS'
export const REGISTER_FAILURE = '@@AUTH/REGISTER_FAILURE'

export function loginProcess() {
    return {
        type: LOGIN_PROCESS as typeof LOGIN_PROCESS
    };
};

export function loginSuccess(user: IUserInfo) {
    return {
        type: LOGIN_SUCCESS as typeof LOGIN_SUCCESS,
        user,
    };
}

export function loginFailure(msg: string) {
    return {
        type: LOGIN_FAILURE as typeof LOGIN_FAILURE,
        msg,
    };
};

export function logout() {
    return {
        type: LOGOUT as typeof LOGOUT
    }
}

export function registerProcess() {
    return {
        type: REGISTER_PROCESS as typeof REGISTER_PROCESS
    };
};

export function registerSuccess(user: IUserInfo) {
    return {
        type: REGISTER_SUCCESS as typeof REGISTER_SUCCESS,
        user
    };
};

export function registerFailure(msg: string) {
    return {
        type: REGISTER_FAILURE as typeof REGISTER_FAILURE,
        msg
    };
};

type AuthActionCreator =
    | typeof loginProcess
    | typeof loginSuccess
    | typeof loginFailure
    | typeof logout
    | typeof registerProcess
    | typeof registerSuccess
    | typeof registerFailure

export type IAuthActions = ReturnType<AuthActionCreator>