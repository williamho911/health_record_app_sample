import { IAuthState, initialState } from './state';
import {
    IAuthActions,
    LOGIN_FAILURE,
    LOGIN_PROCESS,
    LOGIN_SUCCESS,
    LOGOUT,
    REGISTER_FAILURE,
    REGISTER_PROCESS,
    REGISTER_SUCCESS
} from './action';

export const authReducer = (
    state: IAuthState = initialState,
    action: IAuthActions
) => {
    switch (action.type) {
        case LOGIN_PROCESS:
            return {
                ...state,
                isAuthenticated: false,
                isLoginProcessing: true,
                msg: ''
            };
        case LOGIN_SUCCESS:
            return {
                ...state,
                isAuthenticated: true,
                isLoginProcessing: false,
                msg: '',
                user: action.user
            };
        case LOGIN_FAILURE:
            return {
                ...state,
                isAuthenticated: false,
                isLoginProcessing: false,
                msg: action.msg
            };
        case LOGOUT:
            return {
                ...state,
                isAuthenticated: false,
                isLoginProcessing: false,
                msg: ''
            };
        case REGISTER_PROCESS:
            return {
                ...state,
                isAuthenticated: false,
                isLoginProcessing: true
            };
        case REGISTER_SUCCESS:
            return {
                ...state,
                isAuthenticated: true,
                isLoginProcessing: false,
                user: action.user
            };
        case REGISTER_FAILURE:
            return {
                ...state,
                isAuthenticated: false,
                isLoginProcessing: false,
                msg: action.msg
            };
        default:
            return state;
    };
};
