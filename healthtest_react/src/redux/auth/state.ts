export interface IUserInfo {
    id: number,
    email: string,
    first_name: string,
    last_name: string,
    date_of_birth?: Date | string | null,
    policy_code?: string | null,
    genetic_result?: string | null,
};

export interface IAuthState {
    isAuthenticated: boolean | null;
    isLoginProcessing: boolean;
    msg: string;
    user: IUserInfo | null;
};

export const initialState: IAuthState = {
    isAuthenticated: null,
    isLoginProcessing: false,
    msg: '',
    user: null
};