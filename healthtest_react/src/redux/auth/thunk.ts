import { push } from 'connected-react-router';
import { ThunkDispatch } from '../store';
import {
    loginProcess,
    loginSuccess,
    loginFailure,
    logout,
    registerSuccess,
    registerFailure
} from './action';

export function loginThunk(email: string, password: string) {
    return async (dispatch: ThunkDispatch) => {
        dispatch(loginProcess());
        const res = await fetch(
            `${process.env.REACT_APP_API_SERVER}/login`,
            {
                method: 'POST',
                headers: {
                    "Content-type": "application/json",
                },
                body: JSON.stringify({ email, password })
            }
        );

        const result = await res.json();
        if (res.status === 401) {
            dispatch(loginFailure(result.msg));
        } else {
            const user = await JSON.parse(result.user);
            console.log('User from backend: ', user)
            localStorage.setItem('token', result.token);
            localStorage.setItem('payload', user);
            dispatch(loginSuccess(user));
            dispatch(push("/"));
        };
    };
};

export function restoreLoginThunk() {
    return async (dispatch: ThunkDispatch) => {
        const token = localStorage.getItem('token');
        if (!token) {
            dispatch(loginFailure('Access denied without available token'))
        };
        const res = await fetch(
            `${process.env.REACT_APP_API_SERVER}/userInfo`,
            {
                method: 'GET',
                headers: {
                    Authorization: `Bearer ${token}`
                }
            }
        );
        const result = await res.json();
        if (res.status !== 200) {
            dispatch(loginFailure(result.msg));
        } else {
            console.log('Restore: ', typeof result)
            dispatch(loginSuccess(result));
        }
    };
};

export function logoutThunk() {
    return async (dispatch: ThunkDispatch) => {
        localStorage.removeItem('token');
        localStorage.removeItem('payload');
        dispatch(logout());
    }
}

export function registerThunk(user: any) {
    return async (dispatch: ThunkDispatch) => {
        console.log('New register info: ', user)
        const res = await fetch(
            `${process.env.REACT_APP_API_SERVER}/register`,
            {
                method: 'POST',
                headers: {
                    "Content-type": "application/json",
                },
                body: JSON.stringify(user)
            }
        );
        const result = await res.json();
        if (res.status !== 200) {
            dispatch(registerFailure(result.msg));
        } else {
            dispatch(registerSuccess(user));
            localStorage.setItem('token', result.token);
            localStorage.setItem('payload', user);
            dispatch(push("/"));
        };
    };
};