import { applyMiddleware, combineReducers, createStore, compose } from 'redux';
import thunk, { ThunkDispatch as OldThunkDispatch } from 'redux-thunk';
import { RouterState, connectRouter, routerMiddleware, CallHistoryMethodAction } from 'connected-react-router';
import { createBrowserHistory } from 'history';

import { IAuthState } from "./auth/state";
import { IAuthActions } from './auth/action';
import { authReducer } from "./auth/reducer";

declare global {
    /* tslint:disable:interface-name */
    interface Window {
        __REDUX_DEVTOOLS_EXTENSION_COMPOSE__: any;
    }
};

export const history = createBrowserHistory();

export interface IRootState {
    router: RouterState;
    auth: IAuthState;
};

export type IRootAction =
    | CallHistoryMethodAction
    | IAuthActions

const rootReducer = combineReducers<IRootState>({
    router: connectRouter(history),
    auth: authReducer,
});

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export type ThunkDispatch = OldThunkDispatch<IRootAction, null, IRootAction>;

export const store = createStore<IRootState, IRootAction, {}, {}>(
    rootReducer,
    composeEnhancers(
        applyMiddleware(thunk),
        applyMiddleware(routerMiddleware(history))
    )
)